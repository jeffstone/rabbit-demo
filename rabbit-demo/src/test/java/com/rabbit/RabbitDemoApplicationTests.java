package com.rabbit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.rabbit.client.V1Client;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitDemoApplicationTests {

	@Autowired
	private V1Client client;

	@Test
	public void test() throws Exception {
//		do {
//			client.send();
//		} while (true);
		
		for (int i = 0; i < 500; i++) {
			client.send(""+i);
		}
		
		
	}

}
