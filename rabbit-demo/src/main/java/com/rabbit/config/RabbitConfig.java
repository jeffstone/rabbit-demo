package com.rabbit.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate.ConfirmCallback;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class RabbitConfig {

	@Bean
	public Queue v1() {
		return new Queue("v1");
	}

	@Bean
	public Queue v2() {
		return new Queue("v2");
	}
	
//	@Bean
//	public AmqpAdmin amqpAdmin(ConnectionFactory connectionFactory) {
//		RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory);
//		rabbitAdmin.getRabbitTemplate().setConfirmCallback(new ConfirmCallback() {
//			
//			@Override
//			public void confirm(CorrelationData correlationData, boolean ack, String cause) {
//				if (ack) {
//					System.out.println(correlationData.getId());
//				} else {
//					System.out.println(correlationData.getId()+cause);
//				}
//			}
//		});
//		return rabbitAdmin;
//				
//	}

}
