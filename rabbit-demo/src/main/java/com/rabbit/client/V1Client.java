package com.rabbit.client;

import java.util.Date;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class V1Client {
	
	@Autowired
	private AmqpTemplate rabbitTemplate;

	public void send(String i) {
		String context = "hello " + new Date().getTime();
		System.out.println("v1 sender : " + context);
		rabbitTemplate.convertAndSend("v1", i);
	}
}
