package com.rabbit.fanout.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class RabbitConfig {

	@Bean
	public Queue aQueue() {
		return new Queue("fanout-a");
	}

	@Bean
	public Queue bQueue() {
		return new Queue("fanout-b");
	}
	
	@Bean
	public Queue cQueue() {
		return new Queue("fanout-c");
	}
	
	@Bean
	public FanoutExchange fanoutExchange() {
		return new FanoutExchange("fanoutExchange");
	}
	
	@Bean 
	public Binding bingdingExchangeA(FanoutExchange fanoutExchange,Queue aQueue) {
		return BindingBuilder.bind(aQueue).to(fanoutExchange);
	}
	
	@Bean 
	public Binding bingdingExchangeB(FanoutExchange fanoutExchange,Queue bQueue) {
		return BindingBuilder.bind(bQueue).to(fanoutExchange);
	}
	
	@Bean 
	public Binding bingdingExchangeC(FanoutExchange fanoutExchange,Queue cQueue) {
		return BindingBuilder.bind(cQueue).to(fanoutExchange);
	}

}
