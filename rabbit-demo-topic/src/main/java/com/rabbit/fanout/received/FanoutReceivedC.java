package com.rabbit.fanout.received;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues="fanout-c")
public class FanoutReceivedC {

	@RabbitHandler
	public void process(String msg) {
		System.out.println("cQueue"+msg);
	}
}
