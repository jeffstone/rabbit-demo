package com.rabbit.fanout.received;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues="fanout-a")
public class FanoutReceivedA {

	@RabbitHandler
	public void process(String msg) {
		System.out.println("aQueue"+msg);
	}
}
