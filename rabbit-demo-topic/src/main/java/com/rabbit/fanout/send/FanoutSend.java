package com.rabbit.fanout.send;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FanoutSend {

	@Autowired
	private AmqpTemplate amqpTemplate;
	
	public void send() {
		String context = "test fanout send msg";
		System.err.println(context);
		amqpTemplate.convertAndSend("fanoutExchange", "", context);
	}
}
