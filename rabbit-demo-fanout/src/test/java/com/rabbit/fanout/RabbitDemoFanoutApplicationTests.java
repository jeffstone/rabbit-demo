package com.rabbit.fanout;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.rabbit.fanout.send.TopicSend;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitDemoFanoutApplicationTests {
	
	@Autowired
	private TopicSend send;

	@Test
	public void contextLoads() {
		send.send();
	}

}
