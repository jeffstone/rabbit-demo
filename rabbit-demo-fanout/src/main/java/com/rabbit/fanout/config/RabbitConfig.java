package com.rabbit.fanout.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class RabbitConfig {

	@Bean
	public Queue queueMessage() {
		return new Queue("topic.message");
	}

	@Bean
	public Queue queueMessages() {
		return new Queue("topic.messages");
	}
	
	@Bean
	public TopicExchange fanoutExchange() {
		return new TopicExchange("topicExchange");
	}
	
	@Bean 
	public Binding bingdingExchangeA(TopicExchange topicExchange,Queue queueMessage) {
		return BindingBuilder.bind(queueMessage).to(topicExchange).with("topic.message");
	}
	
	@Bean 
	public Binding bingdingExchangeB(TopicExchange topicExchange,Queue queueMessages) {
		return BindingBuilder.bind(queueMessages).to(topicExchange).with("topic.#");
	}
	
}
