package com.rabbit.fanout.send;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TopicSend {

	@Autowired
	private AmqpTemplate amqpTemplate;
	
	public void send() {
		String context = "test topic send msg";
		System.err.println(context);
		amqpTemplate.convertAndSend("topicExchange", "topic.messages", context);
	}
}
