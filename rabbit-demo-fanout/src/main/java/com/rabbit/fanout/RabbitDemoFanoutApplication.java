package com.rabbit.fanout;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitDemoFanoutApplication {

	public static void main(String[] args) {
		SpringApplication.run(RabbitDemoFanoutApplication.class, args);
	}

}
