package com.rabbit.fanout.received;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues="topic.messages")
public class TopicReceivedMessages {

	@RabbitHandler
	public void process(String msg) {
		System.out.println("messages "+msg);
	}
}
